
import Foundation
import AppKit
import CoreWLAN
/*
 networksetup -listallhardwareports, get all the ENs and make a drop down?

 Hardware Port: USB 10/100/1000 LAN
 Device: en5
 Ethernet Address: 00:e1:00:00:14:bc
 */
class Utils {
    static func killOutlook() {
        DispatchQueue.main.async {
            let names = [Utils.OUTLOOK, Utils.MS_UPDATE]
            for task in NSWorkspace.shared().runningApplications {
                if let name = task.localizedName, names.contains(name) {
                    task.forceTerminate()
                }
            }
        }
    }

    static let home_wifi = ["🐢", "wifi"]

    static let FONT_SIZE: CGFloat = 13.0

    static func getTime() -> String {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm:ss.SSS"
        return dateFormatter.string(from: Date())
    }

    static let
        PRINT2 = "'{print $2}'",
        F_SWITCH = "-F\": \"",
        AWK = "awk",
        PIPE = "|",
        NET_SET = "networksetup",
        IP_CONFIG = "ipconfig",
        IF_CONFIG = "ifconfig",
        SUDO = "sudo",
        SET = "set",
        SET_AIRPORT = "-setairportnetwork",
        GET_AIRPORT = "-getairportnetwork",
        ACCESS_CODE = "access-code",
        WIFI_INTERFACE = "en0",
        WIFI_CMD = "-setairportpower",
        WIFI_ON = "on",
        WIFI_OFF = "off",
        GET_IF = "getifaddr",
        ETHERNET_INTERFACE = "en5",
        THUNDERBOLT_INTERFACE = "en10",
        ETHERNET_OFF = "NONE",
        ETHERNET_ON = "DHCP",
        ROBOTO = "Roboto",
        VIA2_HOST = "http://192.168.240.1/",
        VINE_2_WINE = "Vine2Wine",
        VIA_2 = "VIA2",
        WIFI_OFF_STRING = "You are not associated with an AirPort network.",
        WIFI_METHOD = "wifi_is_off",
        V2W_METHOD = "vine_2_wine_is_active",
        VIA2_METHOD = "via_2_is_active",
        AUTH_TEXT = "authorization returned a",
        WIFI_MSG = "Wifi is off\n1) Turn wifi on\n2) Connect to VIA2\n",
        V2W_MSG = "Vine2Wine is on\n1)Switching to VIA2\n",
        VIA2_MSG = "VIA2 is on\n1) Turn Wifi Off\n2) Turn ethernet on\n",
        AUTH_MSG = "Wait for Network to connect then send Auth",
        RETRY_MSG = "Connection Unsuccessful, retrying",
        DHCP_MSG = "Attempting to obtain IP - can take 10 seconds to bind",
        FAIL_MSG = "Unable to connect to {}",
        IP_MSG = "Connection established IP is {}",
        CURRENT_NETWORK_MSG = "Current Wi-Fi Network: ",
        VIA2_CN_MSG = "Attempting to connect to -> VIA2\n",
        DEFAULT_IP = "0.0.0.0",
        SUDO_PATH = "/usr/bin/sudo",
        NEW_LINE = "\n",
        EMPTY = "",
        CONTENT = "Content-Type",
        CONTENT_TYPE = "application/x-www-form-urlencoded",
        ACCEPT = "Accept",
        ACCEPT_TYPE = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,* /*;q=0.8",
        VIA_NAME_KEY = "strName",
        VIA_NAME_VAL = "USER",
        VIA_ACCESS_KEY = "strAccessCode",
        DELIMETER = "------------",
        TURN_ETH_OFF = "Turning Off Ethernet",
        TURN_ETH_ON = "Turning On Ethernet",
        TURN_WIFI_OFF = "Turning Off Wifi",
        TURN_WIFI_ON = "Turning On Wifi",
        VIA2_URL = "ekgnkm/AccessCodePost.asp",
        VIA2_ACCESS_UPDATE = "VIA2 access code updated to",
        OUTLOOK = "Microsoft Outlook",
        MS_UPDATE = "Microsoft Update Assistant",
        CELL = "WifiNameCell",
        PWD = "PWRD",
        LOAD_IFA = "LOADING UP INTERFACES.....",
        IFA_LOADED = "INTERFACES LOADED",
        WIFI_ATTEMPT = "ATTEMPTING TO CONNECT TO",
        WIFI_IP = "Refreshing Wifi IP",
        ETHERNET_IP = "Refreshing Hardware IP"
}

struct NetProperties {
    var msg: String
    var status: String
    var ethernet: Bool
    var powerOn: Bool
    var interface: String
}

enum NetActions: Int {
    case ETH_OFF = 0, ETH_ON = 1, WIFI_OFF = 2, WIFI_ON = 3
    var type: NetProperties {
        switch self {
        case .ETH_OFF:
            return NetProperties(msg: Utils.TURN_ETH_OFF, status: Utils.ETHERNET_OFF, ethernet: true, powerOn: false, interface: Utils.THUNDERBOLT_INTERFACE)
        case .ETH_ON:
            return NetProperties(msg: Utils.TURN_ETH_ON, status: Utils.ETHERNET_ON, ethernet: true, powerOn: true, interface: Utils.THUNDERBOLT_INTERFACE)
        case .WIFI_OFF:
            return NetProperties(msg: Utils.TURN_WIFI_OFF, status: Utils.WIFI_OFF, ethernet: false, powerOn: false, interface: Utils.WIFI_INTERFACE)
        default :
            return NetProperties(msg: Utils.TURN_WIFI_ON, status: Utils.WIFI_ON, ethernet: false, powerOn: true, interface: Utils.WIFI_INTERFACE)
        }
    }
}
