
import RxSwift
import Foundation
import Alamofire
import RxAlamofire
import SystemConfiguration

class API {
    let host = Utils.VIA2_HOST
    let url = Utils.VIA2_URL
    var validated = false
    var returnStatus = false
    var mgr: NetworkReachabilityManager?
    var statusCode = Variable<String>("")
    let headers: HTTPHeaders = [
        Utils.CONTENT: Utils.CONTENT_TYPE,
        Utils.ACCEPT: Utils.ACCEPT_TYPE,
    ]

    // app loop to watch network status
    init() {
        mgr = NetworkReachabilityManager(host: host)
        mgr?.startListening()
        self.mgr?.listener = { status in
            switch status {
            case .notReachable:
                self.returnStatus = false
            case .reachable(Alamofire.NetworkReachabilityManager.ConnectionType.ethernetOrWiFi):
                self.returnStatus = true
            default:
                self.returnStatus = false
            }
        }
    }

    // posts auth code to via2 web service to get
    func postAuth() {
        let accesCode = UserDefaults.standard.integer(forKey: Utils.ACCESS_CODE)
        let parameters: Parameters = [
            Utils.VIA_NAME_KEY: Utils.VIA_NAME_VAL,
            Utils.VIA_ACCESS_KEY: String(accesCode),
        ]

        // creates promise/observable for web request response
        _ = RxAlamofire.request(
            .post,
            "\(self.host)\(self.url)",
            parameters: parameters,
            headers: headers
        )
        // validates it was a good response
        .flatMap({ response in
            return response
                .validate(statusCode: 200 ..< 300)
                .rx
                .responseData()
        })
        .retry(3)
        .timeout(3.0, scheduler: MainScheduler.instance)
        .subscribe(onNext: { response, _ in
            self.statusCode.value = String(response.statusCode)
        })
    }

    func isConnectedToNetwork() -> Bool {
        return (mgr?.isReachable) ?? false
    }
}
