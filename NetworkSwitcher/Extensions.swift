//
//  Table.swift
//  NetworkSwitcher
//
//  Created by Beall, Ryan on 3/8/17.
//
//

import Foundation
import RxSwift
import RxCocoa
import Cocoa
import SystemConfiguration

extension ViewController: NSTableViewDataSource {
    func numberOfRows(in _: NSTableView) -> Int {
        return netCount.value
    }
}

extension ViewController {
    func screenWrite(text: String) {
        let old_txt = self.cmdText.value
        if text != Utils.EMPTY {
            let range = (old_txt.characters.count)
            let delimeter = "\((range == 0) ? Utils.EMPTY : Utils.DELIMETER)"
            let output = "\(old_txt)\(delimeter)\(Utils.NEW_LINE)\(Utils.getTime()) : \(text)\(Utils.NEW_LINE)"
            self.cmdText.value = output
            txtView.insertText(output, replacementRange: NSMakeRange(0, range))
        }
    }

    func executeCommand(path: String = Utils.SUDO_PATH, cmd: [String]?) {
        if let cmd = cmd {
            let ps = Process(),
                psStdOut = Pipe()

            screenWrite(text: "Exec => \(cmd.joined(separator: " "))")

            ps.launchPath = path
            ps.arguments = cmd
            ps.standardOutput = psStdOut
            ps.launch()
            ps.waitUntilExit()
        }
    }
}

extension ViewController: NSTableViewDelegate {
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        if let cell = tableView.make(withIdentifier: Utils.CELL, owner: nil) as? NSTableCellView {
            let networks = Array(net.networks.value)
            cell.textField?.stringValue = networks[row]
            return cell
        }
        return nil
    }

    func selectItem(ssid: String) {
        if let tbl = tblWifiNames, ssid != Utils.EMPTY {
            tbl.deselectAll(nil)
            for (index, item) in net.networks.value.enumerated() {
                if item == ssid {
                    tbl.selectRowIndexes(NSIndexSet(index: index) as IndexSet, byExtendingSelection: true)
                }
            }
        }
    }

    func tableViewSelectionDidChange(_: Notification) {
        let row = tblWifiNames.selectedRow
        if row > -1 {
            let networks = Array(net.networks.value)
            self.selectedNetwork.value = networks[row]
        }
    }
}
