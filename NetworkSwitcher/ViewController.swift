//
//  ViewController.swift
//  NetworkSwitcher
//
//  Created by Beall, Ryan on 1/5/17.
// 🐢
// turtle
// Unicode: U+1F422, UTF-8: F0 9F 90 A2
//

import Foundation
import RxSwift
import RxCocoa
import Cocoa

class ViewController: NSViewController {
    let api = API(),
        net = Networks.sharedInstance,
        defaults = UserDefaults.standard,
        ethStates: [NetActions] = [.ETH_OFF, .ETH_ON],
        wifiStates: [NetActions] = [.WIFI_OFF, .WIFI_ON]

    var netCount = Variable<Int>(0),
        selectedNetwork = Variable<String>(Utils.EMPTY),
        cmdText = Variable<String>(Utils.EMPTY)

    @IBOutlet var tblWifiNames: NSTableView!
    @IBOutlet var btnImgEthernet: NSButton!
    @IBOutlet var btnImgWifi: NSButton!
    @IBOutlet var btnWifiSwitch: NSButton!
    @IBOutlet var btnEthernetSwitch: NSButton!
    @IBOutlet var btnAuth: NSButton!
    @IBOutlet var btnConnect: NSButton!
    @IBOutlet var btnRefresh: NSButton!

    @IBOutlet var lblEthernetIP: NSTextField!
    @IBOutlet var lblWifiIP: NSTextField!
    @IBOutlet var lblAccessCode: NSTextField!
    @IBOutlet var lblNetwork: NSTextField!

    @IBOutlet var imgULine: NSImageView!

    @IBOutlet var txtView: NSTextView!

    @IBOutlet var txtAccessCode: NSTextField! {
        didSet {
            let code = self.defaults.integer(forKey: Utils.ACCESS_CODE)
            if code != 0 {
                txtAccessCode.placeholderString = "\(code)"
            }
            txtAccessCode.font = NSFont(name: Utils.ROBOTO, size: Utils.FONT_SIZE)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tblWifiNames.delegate = self
        tblWifiNames.dataSource = self
        eventHandlers()
        screenWrite(text: Utils.LOAD_IFA)
        DispatchQueue.main.async {
            self.setDisplays()
        }
    }

    // setting the UI for initial load
    func setDisplays() {
        // go out and get all ip addresses
        // blocking on purpose to set switches later
        net.getIFAddresses()

        // looping through setting switches and buttons to on or off
        for (index, interface) in [NetActions.ETH_ON, NetActions.WIFI_ON].enumerated() {
            let _ip = (index == 0) ? net.ethernetIPAddress : net.wifiIPAddress
            let active = (_ip.value != Utils.DEFAULT_IP)

            (index == 0 ? btnEthernetSwitch : btnWifiSwitch)?.state = active ? 1 : 0
            (index == 0 ? btnImgEthernet : btnImgWifi)?.isHidden = !active

            // load network box if wifi on
            if interface == .WIFI_ON {
                self.tblWifiNames.reloadData()
                self.selectItem(ssid: net.currentNetworkName.value)
            }
        }
        self.screenWrite(text: Utils.IFA_LOADED)
    }

    func toggleInterface(interface: NetActions) {
        let hardwired = interface.type.ethernet

        // set the correct method to toggle intreface power
        let toggleInterfacePower = (hardwired ? toggleEthernet : net.toggleWifi)

        screenWrite(text: interface.type.msg)

        // turns the power on or off
        toggleInterfacePower(interface)

        // hidden is the opposite of the power state
        // hidden == true if power == false
        (hardwired ? btnImgEthernet : btnImgWifi)?.isHidden = !interface.type.powerOn

        // if we're turning the power on do stuff
        if interface.type.powerOn {
            screenWrite(text: Utils.DHCP_MSG)
            let delay = hardwired ? 5.0 : 8.0
            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                if hardwired {
                    self.net.getIFAddresses(interface: interface.type.interface)
                } else {
                    self.net.findNetworks()
                }
            }
        }

        // if I'm wifi off clear the ssid box
        if interface == .WIFI_OFF {
            tblWifiNames.deselectAll(nil)
        }
    }

    // refreshes network ssid box
    func refreshNetworkList() {
        net.findNetworks()
        selectItem(ssid: selectedNetwork.value)
        net.getIFAddresses(interface: Utils.WIFI_INTERFACE)
    }

    // connects to the selected network
    func connect() {
        let ssid = selectedNetwork.value

        switch ssid {
        case net.currentNetworkName.value:
            return
        case Utils.VIA_2:
            Utils.killOutlook()
            break
        case Utils.VINE_2_WINE:
            screenWrite(text: "\(Utils.WIFI_ATTEMPT) \(ssid)")
            net.toggleWifi(interface: .WIFI_OFF)
            net.toggleWifi(interface: .WIFI_ON)
            return
        default:
            break
        }

        screenWrite(text: "\(Utils.WIFI_ATTEMPT) \(ssid)")
        net.setNetwork(ssid: ssid)
    }

    // uses Popen to execute, no better API to turn of ethernet
    func toggleEthernet(netType: NetActions) {
        let cmd = [Utils.SUDO, Utils.IP_CONFIG, Utils.SET, Utils.THUNDERBOLT_INTERFACE, netType.type.status]
        executeCommand(cmd: cmd)
        if !(netType.type.powerOn) {
            net.setHWIp()
        }
    }

    func eventHandlers() {
        // observing the list that populates the wifi names
        _ = net.networks.asObservable()
            .subscribe(onNext: { items in
                self.netCount.value = items.count
                self.tblWifiNames.reloadData()
                let isEnabled = (items.count > 0)
                for btn in [self.btnConnect, self.btnRefresh, self.btnAuth] {
                    btn?.isEnabled = isEnabled
                }
            })

        // watches for changes in the access code
        _ = txtAccessCode.rx.text.orEmpty
            .filter({ $0.characters.count > 4 })
            .subscribe(onNext: { txt in
                self.txtAccessCode.placeholderString = Utils.EMPTY
                self.defaults.set(Int(txt), forKey: Utils.ACCESS_CODE)
                self.screenWrite(text: "\(Utils.VIA2_ACCESS_UPDATE) \(txt)")
            })

        // on wifi img click
        _ = btnImgWifi.rx.tap
            .subscribe(onNext: {
                self.screenWrite(text: Utils.WIFI_IP)
                self.net.getIFAddresses(interface: Utils.WIFI_INTERFACE)
            })

        // on ethernet img click
        _ = btnImgEthernet.rx.tap
            .subscribe(onNext: {
                self.screenWrite(text: Utils.ETHERNET_IP)
                self.net.getIFAddresses(interface: Utils.THUNDERBOLT_INTERFACE)
            })

        // notifies on auth status code
        _ = api.statusCode.asObservable()
            .filter({ $0 != Utils.EMPTY })
            .subscribe(onNext: { status in
                self.screenWrite(text: "\(Utils.AUTH_TEXT) \(status)")
            })

        // when network name changes this selects it in the list
        _ = net.currentNetworkName.asObservable()
            .subscribe(onNext: { txt in
                self.selectItem(ssid: txt)
            })

        // emitts the button on off state on tap
        _ = btnEthernetSwitch.rx.tap
            .map({
                return self.ethStates[self.btnEthernetSwitch.state]
            })
            .subscribe(onNext: { state in
                self.toggleInterface(interface: state)
            })

        // emitts the button on off state on tap
        _ = btnWifiSwitch.rx.tap
            .map({
                return self.wifiStates[self.btnWifiSwitch.state]
            })
            .subscribe(onNext: { state in
                self.toggleInterface(interface: state)
            })

        _ = btnConnect.rx.tap
            .subscribe(onNext: {
                self.connect()
            })

        // refreshes network list
        _ = btnRefresh.rx.tap
            .subscribe(onNext: {
                self.refreshNetworkList()
            })

        // posts auth for via2
        _ = btnAuth.rx.tap
            .subscribe(onNext: {
                self.api.postAuth()
            })

        // when wifi ip changes this binds it to the label
        _ = net.wifiIPAddress
            .asObservable()
            .bind(to: lblWifiIP.rx.text)

        // when ethernet ip changes this binds it to the label
        _ = net.ethernetIPAddress
            .asObservable()
            .bind(to: lblEthernetIP.rx.text)
    }
}
