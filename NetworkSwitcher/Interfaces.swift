//
//  NetworkInterface.swift
//  NetworkSwitcher
//
//  Created by Beall, Ryan on 3/3/17.
// http://stackoverflow.com/questions/35806196/display-all-available-wifi-connections-with-swift-in-os-x
// https://developer.apple.com/reference/corewlan/cwinterface
// http://stackoverflow.com/questions/39558868/check-internet-connection-ios-10 hardware

import Foundation
import CoreWLAN
import RxSwift
import RxCocoa

class Networks {
    var currentInterface: CWInterface
    var interfacesNames: [String] = []
    var networks = Variable<Set<String>>([])
    var wifiIPAddress = Variable<String>("")
    var ethernetIPAddress = Variable<String>("")
    var currentNetworkName = Variable<String>("")

    static let sharedInstance: Networks = {
        let instance = Networks()
        return instance!
    }()

    init?() {
        if let defaultInterface = CWWiFiClient.shared().interface(),
            let name = defaultInterface.interfaceName {
            self.currentInterface = defaultInterface
            self.interfacesNames.append(name)
            self.findNetworks()
            _ = self.currentNetworkName.asObservable()
                .filter({
                    $0 != Utils.EMPTY
                })
                .subscribe(onNext: { _ in
                    self.getIFAddresses()
                })

        } else {
            return nil
        }
    }

    func setWiFiIp(incomingIP: String = Utils.DEFAULT_IP) {
        self.wifiIPAddress.value = incomingIP
    }

    func setHWIp(incomingIP: String = Utils.DEFAULT_IP) {
        self.ethernetIPAddress.value = incomingIP
    }

    func setNetworkName(ssid: String = Utils.EMPTY) {
        self.currentNetworkName.value = ssid
    }

    func isWIFIActive() -> Bool {
        return self.currentInterface.powerOn()
    }

    // toggles wifi based off incoming value
    func toggleWifi(interface: NetActions) {
        do {
            try self.currentInterface.setPower(interface.type.powerOn)
            // turn wifi off and reset the vars
            if interface == .WIFI_OFF {
                self.networks.value = Set()
                setNetworkName()
                setWiFiIp()
            }
        } catch let error {
            print(error)
        }
    }

    // gets a list of ssids from scan
    func findNetworks() {
        DispatchQueue.main.async {
            if let list = self.getNetworkList() {
                self.networks.value = Set(list.map({
                    return $0.ssid!
                }))
            }
            self.getNetworkName()
        }
    }

    func getNetworkList() -> Set<CWNetwork>? {
        do {
            return Set(try currentInterface.scanForNetworks(withSSID: nil))
        } catch {
            return nil
        }
    }

    private func connectToNetwork(interface: CWNetwork) -> Bool {
        var PWD: String {
            if let ssid = interface.ssid {
                if Utils.home_wifi.contains(ssid) {
                    if let path = Bundle.main.path(forResource: "private", ofType: "plist"),
                        let dict = NSDictionary(contentsOfFile: path) as? [String: String],
                        let secret = dict[Utils.PWD] {
                        return secret
                    }
                }
            }
            return Utils.EMPTY
        }

        do {
            try self.currentInterface.associate(to: interface, password: PWD)
        } catch {
            return false
        }
        return true
    }

    func setNetwork(ssid: String) {
        if let interfaces = self.getNetworkList() {
            for interface in interfaces {
                if interface.ssid == ssid {
                    if self.connectToNetwork(interface: interface) {
                        setNetworkName(ssid: ssid)
                        break
                    }
                }
            }
        }
    }

    func getNetworkName() {
        self.currentNetworkName.value = Utils.EMPTY
        if let interfaceNames = CWWiFiClient.interfaceNames() {
            for _name in interfaceNames {
                if let interface = CWWiFiClient()?.interface(withName: _name) {
                    if let ssid = interface.ssid() {
                        setNetworkName(ssid: ssid)
                    }
                }
            }
        }
    }

    func getIFAddresses(interface: String = Utils.EMPTY) {
        var interfaces = [interface]

        // reset all the things
        if interface == Utils.EMPTY {
            setWiFiIp()
            setHWIp()
            interfaces = [Utils.THUNDERBOLT_INTERFACE, Utils.WIFI_INTERFACE]
        }

        var addresses = [(String, String)]()

        // Get list of all interfaces on the local machine:
        var ifaddr: UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return }
        guard let firstAddr = ifaddr else { return }

        // For each interface ...
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let name = String(cString: ptr.pointee.ifa_name)

            let flags = Int32(ptr.pointee.ifa_flags)
            var addr = ptr.pointee.ifa_addr.pointee

            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
            if (flags & (IFF_UP | IFF_RUNNING | IFF_LOOPBACK)) == (IFF_UP | IFF_RUNNING) {
                //                || addr.sa_family == UInt8(AF_INET6)
                if addr.sa_family == UInt8(AF_INET), interfaces.contains(name) {

                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))

                    // I honestly dont know how this works,
                    // but it returns a hostname if true
                    if getnameinfo(&addr, socklen_t(addr.sa_len),
                                   &hostname, socklen_t(hostname.count),
                                   nil,
                                   socklen_t(0), NI_NUMERICHOST) == 0 {

                        addresses.append((name, String(cString: hostname)))
                    }
                }
            }
        }

        // no idea what this does
        freeifaddrs(ifaddr)

        // sets the IPs
        for (en, address) in addresses {
            let method = (en == Utils.THUNDERBOLT_INTERFACE) ? setHWIp : setWiFiIp
            method(address)
        }
    }
}
