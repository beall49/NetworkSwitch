//
//  AppDelegate.swift
//  NetworkSwitcher
//
//  Created by Beall, Ryan on 1/5/17.
//
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    var mainWindow: NSWindow!

    func applicationDidFinishLaunching(_: Notification) {
        mainWindow = NSApplication.shared().windows[0]
    }

    func applicationWillTerminate(_: Notification) {
        // Insert code here to tear down your application
    }

    func applicationShouldHandleReopen(_: NSApplication, hasVisibleWindows flag: Bool) -> Bool {
        if !flag {
            mainWindow.makeKeyAndOrderFront(nil)
        }

        return true
    }
}
